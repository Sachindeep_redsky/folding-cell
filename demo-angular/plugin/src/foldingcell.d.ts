
import { EventData, View, Template, KeyedTemplate, Length, Property, CssProperty, Color, Style } from "@nativescript/core/ui/core/view";

export module knownTemplates {
    export const itemTemplate: string;
}

export class FoldingListView extends View {

    public static itemLoadingEvent: string;
    public static itemTapEvent: string;
    public static loadMoreItemsEvent: string;

    android: any /* android.widget.ListView */;
    ios: any /* UITableView */;
    items: any[] | ItemsSource;

    itemTemplate: string | Template;
    itemTemplates: string | Array<KeyedTemplate>;
    itemTemplateSelector: string | ((item: any, index: number, items: any) => string);
    itemIdGenerator: (item: any, index: number, items: any) => number;
    separatorColor: Color;
    backViewColor: Color;
    toggleMode: boolean;
    foldAnimationDuration: number;
    foldsCount: number;
    foldedRowHeight: Length;
    rowHeight: Length;
    iosEstimatedRowHeight: Length;
    refresh();
    scrollToIndex(index: number);
    isItemAtIndexVisible(index: number): boolean;
    on(eventNames: string, callback: (data: EventData) => void, thisArg?: any);
    on(event: "itemLoading", callback: (args: ItemEventData) => void, thisArg?: any);
    on(event: "tap", callback: (args: ItemEventData) => void, thisArg?: any);
    on(event: "itemTap", callback: (args: ItemEventData) => void, thisArg?: any);
    on(event: "loadMoreItems", callback: (args: EventData) => void, thisArg?: any);
}

export interface ItemEventData extends EventData {

    index: number;
    view: View;
    ios: any /* UITableViewCell */;
    android: any /* android.view.ViewGroup */;
}

export interface ItemsSource {
    length: number;
    getItem(index: number): any;
}

export interface TemplatedItemsView {
    items: any[] | ItemsSource;
    itemTemplate: string | Template;
    itemTemplates?: string | Array<KeyedTemplate>;
    refresh(): void;
    on(event: "itemLoading", callback: (args: ItemEventData) => void, thisArg?: any);
    off(event: "itemLoading", callback: (args: EventData) => void, thisArg?: any);
}

export const itemsProperty: Property<FoldingListView, any[] | ItemsSource>;
export const itemTemplateProperty: Property<FoldingListView, string | Template>;
export const itemTemplatesProperty: Property<FoldingListView, string | Array<KeyedTemplate>>;
export const separatorColor: Property<FoldingListView, Color>;
export const rowHeightProperty: Property<FoldingListView, Length>;
export const iosEstimatedRowHeightProperty: Property<FoldingListView, Length>;
export const separatorColorProperty: CssProperty<Style, Color>;

export class ForegroundView extends View {

}


export class ContainerView extends View {

}