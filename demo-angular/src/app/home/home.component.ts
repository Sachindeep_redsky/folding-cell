import { Component, OnInit } from "@angular/core";
import { registerElement } from "nativescript-angular/element-registry";
import { ListView } from "@nativescript/core/ui/list-view/list-view";
import { isAndroid, Page } from "tns-core-modules/ui/page/page";

declare var UIView: any;
declare var Foldingcell: any;
declare var NSIndexPath: any;

@Component({
    selector: "Home",
    templateUrl: "./home.component.html",
})
export class HomeComponent implements OnInit {
    items;

    isExpanded: boolean;

    constructor(private page: Page) {
        this.isExpanded = false;
        this.items = [];
        this.page.actionBarHidden=true;
    }

    ngOnInit(): void {
        this.items = [
            { g: 1 },
            { g: 1 },
            { g: 1 }
        ];
    }

    onLoaded(args: any) {
    }

    onListLoaded(args: any) {
    }

    onTap(args: any) {
    }

    onItemTap(args) {
    }

}
